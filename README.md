# Proyecto de Modificación de BBDD - SQL en Oracle Database 21c Express Edition
Script desarrollado entre varios alumnos como parte de la asignatura **[42328	- Desarrollo de Bases de Datos](https://guiae.uclm.es/vistaGuia/407/42328)** en el tercer curso de ingeniería informática, de la rama de ingeniería del software. En este trabajo, se han añadido disparadores, funciones y cursores a una BBDD entregada por el profesorado.

## Tecnologías Utilizadas
**Lenguaje de programación**: SQL  
**Herramientas de desarrollo**: Oracle Database 21c Express Edition 

Más información en los PDF's asociados.
